import React from 'react';
import {Route, Switch, Redirect} from 'react-router-dom';
import AdministrationPage from '../AuthenticatedComponents/AdministrationPage';
import {useSelector} from 'react-redux';
import {selectUserInfo} from '../redux/actions/index'
import UserPage from '../AuthenticatedComponents/UserPage';
import BasketReview from '../AuthenticatedComponents/BasketReview';
import CompanyData from '../AuthenticatedComponents/CompanyData'
import PersonalData from '../AuthenticatedComponents/PersonalData'
import {selectCompany} from '../redux/actions/indexCompany';
import DeliveryRates from '../AuthenticatedComponents/DeliveryRates';
import SuccessPayment from '../Access/SuccessPayment'
import Configuration from '../Access/Configuration';




function AuthRoute(){

    const userInfo=useSelector(selectUserInfo)
  

   

    return(
        <div>

        {userInfo.groupUser==='Admin' ?
        <div>
        <Route exact path="/administrationpage" component={AdministrationPage}/>
        <Route exact path="/companyData" component={CompanyData}/>
        <Route exact path="/deliveryRates" component={DeliveryRates}/>
        <Route exact path="/configuration" component={Configuration}/>
        <Redirect from="*" to="/administrationpage"></Redirect>
        </div>
         :
        <div>
        <Route exact path="/personalData" component={PersonalData}/>
        <Route exact path="/userPage" component={UserPage}/>
        <Route exact path="/basketReview" component={BasketReview}/>
        <Route exact path="/successPayment" component={SuccessPayment}/>
        
        <Redirect from="*" to="/userPage"></Redirect>
        </div>  
        }
        </div>

    )
}

export default AuthRoute;