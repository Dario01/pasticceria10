import axios from 'axios';
import {useDispatch} from 'react-redux';
import {setToken} from '../redux/reducers/authSliceReducerToolkit'
import store from '../redux/store/store'
import {logout} from '../redux/actions/index'
//Add a request interceptor



const instance=axios.create({})


instance.interceptors.request.use(config=>{

 
     
        const token=localStorage.getItem('token')
     
        if(token){
            config.headers['Authorization']=token
            
        }
     
        config.headers['Content-Type']='application/json'
     
      
     
            return config
     
     }, error=>{
     
    
        Promise.reject(error)
     });
     
    

     instance.interceptors.response.use(response=>{
      
        return response
     
     }, error=>{
        const dispatch=store.dispatch
        const originalRequest=error.config

        if(error.response.data.error==='Invalid token'){
     
          
            dispatch(logout())
            
     
        }
        return Promise.reject(error)
     });

    export default instance;