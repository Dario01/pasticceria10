import {setDataCompany, setConfirmDataCompany, setDataClickSet,setDataCompanyLogin} from '../reducers/companyDataSliceReducerToolkit';
import instance from '../../HttpService/axiosInterceptor';

const axios=instance;

export const changeDataCompany=(data)=>dispatch=>{


dispatch(setDataCompany(data))

}

export const confirmDataCompany=(data)=>dispatch=>{



    axios.post("/api/companyData",{
        body:data
    }).then(res=>{
      
        dispatch(setConfirmDataCompany())

    })

}

export const setDataClick=()=>dispatch=>{

    

    dispatch(setDataClickSet())
}

export const autoDataCompany=()=>dispatch=>{

    axios.get("/api/companyData").then(res=>{

      

        if(res.data.result!=="No data company"){


            return dispatch(setDataCompanyLogin(res.data.result[0]))
        }

    })

}





export const selectCompany=(state)=>state.companyData.dataCompany;