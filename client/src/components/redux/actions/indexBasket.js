import Axios from 'axios';
import {setAddBasket, setRemoveBasket, setRemoveBasketReview, setBasketEmpty} from '../reducers/basketSliceReducerToolkit';
import {setHomeBasketRemove,setHomeBasketAdd} from '../reducers/homeSliceReducerToolkit';
import instance from '../../HttpService/axiosInterceptor';

const axios=instance


export const addToBasket=(elem)=>dispatch=>{

  

    dispatch(setAddBasket(elem))
    dispatch(setHomeBasketRemove(elem))

}

export const removeFromBasket=(elem)=>dispatch=>{


    dispatch(setRemoveBasket(elem))
    dispatch(setHomeBasketAdd(elem))
}

export const removeFromBasketReview=(elem,choice,totalprice,qty)=>dispatch=>{

    dispatch(setRemoveBasketReview({name:elem, choice:choice, totalprice:totalprice, qty:qty}))
    dispatch(setHomeBasketAdd({name:elem,choice:choice}))

}

export const handlePayments=(token,totalAmount,basket, history)=>dispatch=>{

  
 

    axios.post("/api/stripe",{
        body:{token,totalAmount,basket}
    }).then((res)=>{
     
        dispatch(setBasketEmpty())
        history.push("/successPayment")
    })



}





export const selectHome=(state)=>state.home.home
export const selectTotal=(state)=>state.basket.basket.reduce((sum,ele)=>sum+ele.qty,0)
export const selectTotalAmount=(state)=>state.basket.basket.reduce((sum,ele)=>sum+ele.totalprice,0)
export const selectBasket=(state)=>state.basket.basket