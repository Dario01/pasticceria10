


import {setConfigurationData,
    setConfigurationDataResult,
    setChangeModifyEnable,
    setChangeModifyDisable,
    setGetConfigurationDataResult,
    setConfigurationDataUpdate,
    setConfigurationDataUpdateResult,
    setConfigurationCopyData,
    setChangeModifyDisableAll,
    setChangeUpdateReset,
    setConfigurationDataReset} from '../reducers/configurationDataSliceReducerToolkit';
import instance from '../../HttpService/axiosInterceptor';


const axios=instance;


export const changeDataConfiguration=value=>dispatch=>{





dispatch(setConfigurationData(value))

}

export const sendModifyData=value=>dispatch=>{


    axios.post("/api/configurationData",{
        body:value
    }).then(resp=>{
      


        dispatch(setChangeModifyDisableAll())
        dispatch(setConfigurationCopyData())
        dispatch(setChangeUpdateReset())

    }).catch(err=>{
        console.log('err' ,err)
    })
}

export const sendConfiguration=value=>dispatch=>{




axios.post("/api/configurationData",{
    body:value.configurationData
}).then(res=>{



    dispatch(setConfigurationDataResult(res.data.result))
    dispatch(setConfigurationCopyData())
    dispatch(setConfigurationDataReset())

}).catch(err=>{
    console.log('err configuration',err)
})



}

export const changeModifyEnable=value=>dispatch=>{

dispatch(setChangeModifyEnable(value))


}

export const changeModifyDisable=value=>dispatch=>{

    dispatch(setChangeModifyDisable(value))

}

export const changeConfiguration=value=>dispatch=>{

dispatch(setConfigurationDataUpdate(value))
dispatch(setConfigurationDataUpdateResult(value))

}

export const autoReadingConfiguration=()=>dispatch=>{



axios.get("/api/configurationData").then(rese=>{
  

    dispatch(setGetConfigurationDataResult(rese.data.result))
    dispatch(setConfigurationCopyData())


}).catch(err=>{
    console.log('ERR AUTOREADING',err)
})





}

export const selectConfiguration=(state)=>state.configurationData;