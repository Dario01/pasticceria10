import {SET_ERROR_SALES, 
    SET_SALESCAKE,
    SET_SALESCAKE_PRICE, 
    SET_SALESCAKE_PRICE_ENABLE,
    SET_SALESCAKE_ELEMENT_ERROR,
    SET_SALESPRICE_ELEMENT,
    SET_SALESPRICE_INSERT_RESULT} from '../reducers/salesType'
import {SET_HOME_SALES,SET_SALESINFO_DISABLE} from '../reducers/homeType'
//import axios from 'axios';
import { SET_CAKE } from '../reducers/cakeType';

//**REDUX TOOLKIT */

import {useDispatch} from 'react-redux';
import {setHomeSales} from '../reducers/homeSliceReducerToolkit'
import {setSalesCake,
        setErrorSales,
        setSalesPriceElement,
        setSalesCakePriceEnable,
        setSalesCakePrice,
        setSalesCakeElementError,
        setSalesPriceInsertResult,
        setInsertSalesEmpty,
        setCheckFalseCake,
        setResultInsertEmpty,
        setActivationChecked,
        setActivationProduction,
        setChangeSalesPriceElement,
        setFilterSales,
        setActivationResponse,
        setSalesActivationEmpty} from '../reducers/saleSliceReducerToolkit';
import {setSalesInfoDisable} from '../reducers/homeSliceReducerToolkit'

import instance from '../../HttpService/axiosInterceptor'

//** */




// export const autoReadingSales=()=>dispatch=>{

//     const headers={
//         'Content-type':'application/json',
//         'authorization': localStorage.getItem('token')
//     }
    
//     axios.get("/api/cake",{
//         headers:headers
//     }).then(response=>{
//         console.log('in response sales',response)
//     dispatch({type:SET_SALESCAKE, payload:response.data.result})
//     dispatch({type:SET_ERROR_SALES, payload:''})
//     })
//     .catch(error=>{
//         console.log('IN CATCH')
//         dispatch({type:SET_ERROR_SALES, payload:error.response.data.result})
//     })

// }


// export const insertSalesCakeAction=(value)=>dispatch=>{
//     console.log('value data')

// dispatch({type:SET_SALESCAKE_PRICE, payload:{name:value.name, price:value.value, id:value.id}})
  
// }

// export const changeInsertPrice=(value)=>dispatch=>{

// console.log('VALUE INSERT',value.checked)
//     dispatch({type:SET_SALESCAKE_PRICE_ENABLE, payload:value})
// }

// export const insertCakeInSales=(value)=>dispatch=>{

//         console.log('value',value)

//         let arr=[]

//     value.map(val=>{
//         console.log('vali in map', val)
//         if(val.first===undefined || val.first===""){
//             arr.push(val.name)
//         }

//         return val

//     })

//     if(arr[0]!==undefined){
       
        

//         return    dispatch({type: SET_SALESCAKE_ELEMENT_ERROR, 
//                 payload:{name:arr, error:'You have to insert at least a first price'}})

        
//     }

//     console.log('OUT OF IF STATEMENT')

//     const headers={
//         'Content-type':'application/json',
//         'authorization':localStorage.getItem('token')
//     }

//     axios.post("/api/salespricelist",{
//         headers:headers,
//         body:value
//     }).then(response=>{
//             console.log('RESPOSNE RESULT SALESPRICE ELEMENT',response)
//         dispatch({type:SET_SALESPRICE_INSERT_RESULT, payload:response.data.result})
//         dispatch({type:SET_SALESPRICE_ELEMENT, payload:response.data.sales})
        
//     })
//     .catch(error=>console.log('error sales',error.response))


// }

// export const autoReadingSalesPrice=()=>dispatch=>{

//     const headers={
//         'Content-type':'application/json',
//         'authorization':localStorage.getItem('token')
//     }

//     axios.get("/api/salespricelist",{
//         headers:headers
//     }).then(response=>{
//         console.log('response sales price',response.data.sales)
//         dispatch({type:SET_SALESPRICE_ELEMENT, payload:response.data.sales})
        
//     })
//     .catch(error=>console.log('ERROR RESOPNSE',error))
// }

// // export const autoReadingSalesHome=()=>dispatch=>{


// //     axios.get("/reading/saleshome").then(response=>dispatch({type:SET_HOME_SALES, payload:response.data.sales}))
// //     .catch(error=>console.log('ERROR IN RESPONSE',error.response.data))
// // }

// //**REDUX TOOLKIT */

// export const autoReadingSalesHome=()=>dispatch=>{

//     console.log('AUTOREADINGSALES HOME')

    
    


//     axios.get("/reading/saleshome").then(response=>dispatch(setHomeSales(response.data.sales)))

// }

// //** */

// export const setDisableInfo=(value)=>dispatch=>{


//     dispatch({type:SET_SALESINFO_DISABLE, payload:value})

// }


//**REDUX TOOLKIT */

const axios=instance

export const autoReadingSales=()=>dispatch=>{

    // const headers={
    //     'Content-type':'application/json',
    //     'authorization': localStorage.getItem('token')
    // }
    
    axios.get("/api/cake").then(response=>{
      
    dispatch(setSalesCake(response.data.result))
    dispatch(setInsertSalesEmpty())
    dispatch(setResultInsertEmpty())
    dispatch(setErrorSales({error:''}))
    })
    .catch(error=>{
      
        dispatch(setErrorSales(error.response.data.result))
    })

}


export const insertSalesCakeAction=(id,value,name)=>dispatch=>{
   

dispatch(setSalesCakePrice({id:id, name:name, value:value}))
  
}

export const changeInsertPrice=(checked,id)=>dispatch=>{


    dispatch(setSalesCakePriceEnable({checked:checked,id:id}))
}

export const insertCakeInSales=(insertSales)=>dispatch=>{

  

        let arr=[]

    insertSales.map(val=>{
   
        if(val.first===undefined || val.first==="" || val.daily_qty===undefined || val.daily_qty===""){
         
            arr.push(val.id)
           
        }

        return val

    })

    if(arr[0]!==undefined){

    
        return dispatch(setSalesCakeElementError({arrId:arr,errorFirstPrice:'You have to insert at least a first price or insert a daily production'}))
    }else{



  

    axios.post("/api/salespricelist",{
        
        body:insertSales
    }).then(response=>{
        
        dispatch(setSalesPriceInsertResult(response.data.result))
        dispatch(setSalesPriceElement(response.data.sales))
        dispatch(setFilterSales(response.data.result))
        dispatch(setCheckFalseCake(false))
        dispatch(setInsertSalesEmpty())
        
    })
    .catch(error=>console.log('error sales',error.response))

}

}

export const autoReadingSalesPrice=()=>dispatch=>{

  

    axios.get("/api/salespricelist").then(response=>{
      
        dispatch(setSalesPriceElement(response.data.sales))
        
    })
    .catch(error=>console.log('ERROR RESPONSE',error))
}


export const autoReadingSalesHome=()=>dispatch=>{

   


    axios.get("/reading/saleshome").then(response=>{
   
        dispatch(setHomeSales(response.data.stocks))})

}

//** */

export const setDisableInfo=(value)=>dispatch=>{
   

    dispatch(setSalesInfoDisable(value))

}

export const changeActivationChecked=(value)=>dispatch=>{

 
    dispatch(setActivationChecked(value))



}

export const changeActivationProductionSchedule=value=>dispatch=>{

   


    dispatch(setActivationProduction(value))
    dispatch(setChangeSalesPriceElement(value))



}

export const sendActivation=value=>dispatch=>{

   

        axios.post("/api/salesActivation",{
            body:value
        }).then(res=>{
           

            dispatch(setActivationResponse({response:res.data.response, activated:value}))
            dispatch(setSalesActivationEmpty())


        }).catch(err=>console.log('err',err))


}


export const selectSale=(state)=>state.sale;

//** */