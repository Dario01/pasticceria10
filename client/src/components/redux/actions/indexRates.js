import { setErrorRate,setRatesData,setRatesDataEmpty,setRatesDataReview,setRatesDeleted, setErrorEmpty } from "../reducers/ratesDataSliceReducerToolkit"
import instance from '../../HttpService/axiosInterceptor'

const axios=instance


export const changeDataRates=(data)=>dispatch=>{

    dispatch(setRatesData(data))

}

export const insertDataRates=(data)=>dispatch=>{



    axios.post("/api/ratesData",{
        body:data
    }).then(res=>{
     

        dispatch(setRatesDataEmpty())
        dispatch(setRatesDataReview(res.data))
        dispatch(setErrorEmpty())
    }).catch(err=>{
       

        dispatch(setErrorRate(err.response.data))

    })

}

export const deleteRate=(id)=>dispatch=>{

    axios.delete(`/api/ratesData/${id}`).then(res=>{
     
        dispatch(setRatesDeleted(id))
    })


}

export const autoLoadRates=()=>dispatch=>{

    axios.get("/api/ratesData").then(res=>{
       
        dispatch(setRatesDataReview(res.data))
        dispatch(setRatesDataEmpty())
        dispatch(setErrorEmpty())
    })


}





export const selectRates=state=>state.rates.ratesData;
export const selectRatesReview=state=>state.rates.ratesDataReview;
export const selectRatesLoading=state=>state.rates.loaded;
export const selectErrorRate=state=>state.rates.error