import {setPersonalData} from '../reducers/personalDataReducerToolkit';
import {setEnableModifyConfirmed,setUserInfoChange} from '../reducers/authSliceReducerToolkit';
import instance from '../../HttpService/axiosInterceptor'


const axios=instance

export const confirmUserData=(userInfo)=>dispatch=>{

    axios.put(`/api/personalData/${userInfo.id}`,{
        body:userInfo
    }).then(res=>{
      
       dispatch(setEnableModifyConfirmed())
    })

   



}

export const changeDataPersonal=(e)=>dispatch=>{



    dispatch(setUserInfoChange(e))
}

export const selectPersonalData=state=>state.personalData.dataUser

