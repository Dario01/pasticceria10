import {createSlice} from '@reduxjs/toolkit'




const homeSlice=createSlice({
    name:'homeSlice',
    initialState:{
        home:[]
    },
    reducers:{

        setHomeSales(state,action){
            const homePayload=action.payload

            homePayload.map(eleme=>{

                         if(state.home.find((elem)=>eleme.nameCake===elem.name)){

                            state.home.map(elements=>{

                         

                                if(eleme.nameCake===elements.name){


                                    if(eleme.choice===1){

                                        
                                            
                                     return elements.first={price:eleme.price, qty:eleme.qty, initialQty:eleme.qty}
                                            
                                    
                                      
                                   

                                    }
                                
                                if(eleme.choice===2){

                                 
                                    
                                 elements.second={price:eleme.price, qty:eleme.qty, initialQty:eleme.qty}
                                    
                                }
                                if(eleme.choice===3){

                                   
                                       
                                 elements.third={price:eleme.price, qty:eleme.qty, initialQty:eleme.qty}
                                        
                                
                            }
                        }

                            })

                            
                            
                            }  else{

                                if(eleme.choice===1){

                                  

                                    state.home.push({ cakeId:eleme.list[0]._id, 
                                     name:eleme.nameCake,
                                      imgLink:eleme.list[0].imgLink,
                                      first:{price:eleme.price, qty:eleme.qty, initialQty:eleme.qty},
                                       ingredients:eleme.list[0].ingredients,
                                       weight:eleme.list[0].weight,
                                       disableInfo:false,
                                    })
 
 
                                 }
                                 if(eleme.choice===2){

                                  
 
                                     state.home.push({ cakeId:eleme.list[0]._id, 
                                         name:eleme.nameCake,
                                          imgLink:eleme.list[0].imgLink,
                                          second:{price:eleme.price, qty:eleme.qty, initialQty:eleme.qty},
                                           ingredients:eleme.list[0].ingredients,
                                           weight:eleme.list[0].weight,
                                           disableInfo:false,
                                        })
 
                                 }
                                 if(eleme.choice===3){

                                 
 
                                     state.home.push({ cakeId:eleme.list[0]._id, 
                                         name:eleme.nameCake,
                                          imgLink:eleme.list[0].imgLink,
                                          third:{price:eleme.price, qty:eleme.qty, initialQty:eleme.qty},
                                           ingredients:eleme.list[0].ingredients,
                                           weight:eleme.list[0].weight,
                                           disableInfo:false,
                                        })
 
                                 }
                            }
                            

                                

                            })

         



            

    },
    setSalesInfoDisable(state,action){
        const elemInfoDisablePayload=action.payload
        console.log(elemInfoDisablePayload)

        state.home=state.home.map(elem=>{
            
            if(elem.name===elemInfoDisablePayload){
                
                elem.disableInfo=!elem.disableInfo
            }

            return elem
        })


    },

    setHomeBasketRemove(state,action){
        const homebasketPayload=action.payload
       

        state.home.map(elem=>{
            if(elem.name===homebasketPayload.name){

                if(homebasketPayload.choice===1){

                    if(elem.first.qty>0){

                 return   elem.first.qty-=1

                }
            }
                if(homebasketPayload.choice===2){

                    if(elem.second.qty>0){

                  return  elem.second.qty-=1

                }
            }
                if(homebasketPayload.choice===3){

                    if(elem.third.qty>0){

                 return   elem.third.qty-=1

                }
            }



            }
        })

    },
    setHomeBasketAdd(state,action){
        const homebasketPayload=action.payload;

      


        state.home.map(elem=>{
            if(elem.name===homebasketPayload.name){

                  

                if(homebasketPayload.choice===1){

                   

                        return   elem.first.qty+=1
       
                       
                }
                if(homebasketPayload.choice===2){



                 

                 return   elem.second.qty+=1

                

                }
                if(homebasketPayload.choice===3){


               

                 return   elem.first.qty+=1

                

                }



            }
        })



    }

}
})

export const {setHomeSales}=homeSlice.actions;
export const {setSalesInfoDisable}=homeSlice.actions;
export const {setHomeBasketRemove}=homeSlice.actions;
export const {setHomeBasketAdd}=homeSlice.actions;

export default homeSlice.reducer