import {createSlice} from '@reduxjs/toolkit';


const saleSlice=createSlice({
    name:'saleSlice',
    initialState:{
        sales:[],
        salesPriceElement:[],
        errors:undefined,
        insertSales:[],
        resultInsert:[],
        salesActivation:[],
        activationResponse:[]


    },
    reducers:{
        setSalesCake(state,action){
            const salesCakePayload=action.payload;
            state.sales=salesCakePayload.map(sales=>{
                return {
                    id:sales._id,
                    name:sales.name,
                    imgLink:sales.imgLink,
                    activated:false,
                    enableSetPrice:false
                }
            })

        },

        setActivationChecked(state,action){

            const checkedPayload=action.payload

 


            state.insertSales.forEach(elem=>{

                if(elem.id===checkedPayload.id){

                    elem.activated=!elem.activated
                }

                return elem

            })



        },
        setErrorSales(state,action){
            const salesErrorPayload=action.payload;
            return {
                ...state, errors:salesErrorPayload
            }

        },
        setSalesPriceElement(state,action){

            const salesPriceElementPayload=action.payload

           
            state.salesPriceElement=salesPriceElementPayload

         


        },
        setSalesCakePriceEnable(state,action){

            const salesCakePriceEnablePayload=action.payload;

           if(salesCakePriceEnablePayload.checked){
               state.sales.map(sale=>{
                   if(salesCakePriceEnablePayload.id===sale.id){
                        sale.enableSetPrice=!sale.enableSetPrice
                        state.insertSales.push({id:sale.id, activated:false})
                   }
                   return sale
               })
           }else{
               state.sales=state.sales.map(sale=>{
                   if(sale.id===salesCakePriceEnablePayload.id){
                       sale.enableSetPrice=!sale.enableSetPrice
                       state.insertSales=state.insertSales.filter(saleIn=>saleIn.id!==salesCakePriceEnablePayload.id)
                   }
                   return sale
               })
           }


        },
        setSalesCakePrice(state,action){
            const salesCakePricePayload=action.payload;


            state.insertSales.forEach(sale=>{
                if(sale.id===salesCakePricePayload.id){
                    sale[salesCakePricePayload.name]=salesCakePricePayload.value
                }
            
                return sale
            })

        },
        setSalesCakeElementError(state,action){
            const salesCakeElementErrorPayload=action.payload;

            state.insertSales.forEach(sale=>{
                salesCakeElementErrorPayload.arrId.forEach(saleId=>{
                if(sale.id===saleId){
                    sale.errorFirstPrice=salesCakeElementErrorPayload.errorFirstPrice
                }
                return saleId

            })

            return sale
            })


        },
        setSalesPriceInsertResult(state,action){

           state.resultInsert=[]
            const salesPriceInsertResultPayload=action.payload;
           

            

            salesPriceInsertResultPayload.forEach(sale=>{
                state.resultInsert.push(sale.name)

            })


        },
        setInsertSalesEmpty(state,action){

            state.insertSales=[]


        },
        setResultInsertEmpty(state,action){
            state.resultInsert=[]


        },
        setCheckFalseCake(state,action){

            state.sales.forEach(sale=>{

                if(sale.enableSetPrice){
                    sale.enableSetPrice=false
                }
                return sale

            })

        },
        setActivationProduction(state,action){

            const activationPayload=action.payload



  

            if(state.salesActivation.length===0){

                state.salesActivation.push({id:activationPayload.id, name:activationPayload.name, checked:activationPayload.checked})
            }else{
                state.salesActivation.find((elem)=>elem.id===activationPayload.id) ? 

                state.salesActivation.map(ele=>{
                    if(ele.id===activationPayload.id){

                        ele.checked=activationPayload.checked
                    }
                    return ele
                })


                    :
                    state.salesActivation.push({id:activationPayload.id, name:activationPayload.name, checked:activationPayload.checked})
                
            }



        },
        setChangeSalesPriceElement(state,action){

            const salesElementPayload=action.payload
       

            state.salesPriceElement=state.salesPriceElement.map(elem=>{
            
                if(elem.cakeId===salesElementPayload.id){
                    elem.sales=!elem.sales
                }
                return elem
            })
        },
        setFilterSales(state,action){

            const salesFilterPayload=action.payload;

            salesFilterPayload.map(ele=>{
                state.sales=state.sales.filter(el=>el.name!==ele.name)
            })

        
        },
        setActivationResponse(state,action){

            state.activationResponse=[]

            const responsePayload=action.payload;

          

            responsePayload.activated.map(el=>{
                state.activationResponse.push(el.name)
            })

        },
        setSalesActivationEmpty(state,action){

            state.salesActivation=[]
        }

        

    }



})

export const {setSalesCake}=saleSlice.actions;
export const {setErrorSales}=saleSlice.actions;
export const {setSalesPriceElement}=saleSlice.actions;
export const {setSalesCakePriceEnable}=saleSlice.actions;
export const {setSalesCakePrice}=saleSlice.actions;
export const {setSalesCakeElementError}=saleSlice.actions;
export const {setSalesPriceInsertResult}=saleSlice.actions;
export const {setInsertSalesEmpty}=saleSlice.actions;
export const {setCheckFalseCake}=saleSlice.actions;
export const {setResultInsertEmpty}=saleSlice.actions;
export const {setActivationChecked}=saleSlice.actions;
export const {setActivationProduction}=saleSlice.actions
export const {setChangeSalesPriceElement}=saleSlice.actions;
export const {setFilterSales} = saleSlice.actions;
export const {setActivationResponse}=saleSlice.actions;
export const {setSalesActivationEmpty}=saleSlice.actions



export default saleSlice.reducer;