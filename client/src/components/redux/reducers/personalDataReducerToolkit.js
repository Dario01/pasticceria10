import {createSlice} from '@reduxjs/toolkit'


const personalDataSlice=createSlice({
    name:'personalDataSlice',
    initialState:{
        dataUser:{email:'',
        state:'',
        postalCode:'',
        country:'',
        city:'',
        deliveryAddress:'',
        mainAddress:''
    }
    },
    reducers:{
        setPersonalData(state,action){

            const dataUserPayload=action.payload

            state.dataUser={...state.dataUser, [dataUserPayload.name]:dataUserPayload.value}


        }
    }
})

export const {setPersonalData}=personalDataSlice.actions


export default personalDataSlice.reducer