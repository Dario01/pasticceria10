import { createSlice } from "@reduxjs/toolkit";

const basketSlice = createSlice({
  name: "basketSlice",
  initialState: {
    basket: [],
    
  },
  reducers: {
    setAddBasket(state, action) {
      const elemSelectedPayload = action.payload;

      

      

      if (state.basket[0] === undefined) {
     
        state.basket.push(elemSelectedPayload);
        
       
      } else {
        if (
          state.basket.find(
            (elem) =>
              elem.name === elemSelectedPayload.name &&
              elem.choice === elemSelectedPayload.choice
          )
        ) {
          state.basket.map((elem) => {
            if (
              elem.name === elemSelectedPayload.name &&
              elem.choice === elemSelectedPayload.choice
            ) {
            
             

              elem.qty += elemSelectedPayload.qty;
              elem.totalprice += elemSelectedPayload.totalprice;
              
            }

            return elem;
          });
        } else {
        
        
          state.basket.push(elemSelectedPayload);
          
          
        }
      }
    },
    setRemoveBasket(state, action) {
      const elemRemovePayload = action.payload;

      

      state.basket.find(
        (elem) =>
          elem.name === elemRemovePayload.name &&
          elem.choice === elemRemovePayload.choice &&
          elem.qty < 2
      )
        ? state.basket = state.basket.filter(
            (ele) =>
              ele.name !== elemRemovePayload.name ||
              ele.choice !== elemRemovePayload.choice,
            
          )
        : state.basket.map((eleme) => {
           

            if (
              eleme.name === elemRemovePayload.name &&
              eleme.choice === elemRemovePayload.choice
            ) {
          
              eleme.qty -= elemRemovePayload.qty;
              eleme.totalprice -= elemRemovePayload.totalprice;
              
            }
            return eleme;
          });
    },

    setRemoveBasketReview(state,action){

        const basketReviewPayload=action.payload

    

        if(basketReviewPayload.qty>1){

        

        var priceuni=basketReviewPayload.totalprice/basketReviewPayload.qty

      

        state.basket.map(elem=>{

                if(elem.name===basketReviewPayload.name && elem.choice===basketReviewPayload.choice){
                    elem.qty-=1
                    elem.totalprice-=priceuni
                }

                return elem

        }
        )

    }else{
    
        state.basket=state.basket.filter(elem=>elem.name!==basketReviewPayload.name || elem.choice!==basketReviewPayload.choice)


    }




    },
    setBasketEmpty(state,action){
      state.basket=[]
    }
  },
});

export const { setAddBasket, setRemoveBasket, setRemoveBasketReview,setBasketEmpty } = basketSlice.actions;

export default basketSlice.reducer;
