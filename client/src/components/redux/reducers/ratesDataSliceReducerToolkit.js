import {createSlice} from '@reduxjs/toolkit';


const ratesSlice=createSlice({
    name:'ratesSlice',
    initialState:{
        ratesData:{
            from:'',
            to:'',
            price:0
        },
        ratesDataReview:[],
        error:'',
        loaded:false
    },
    reducers:{
        setRatesData(state,action){
           const ratesDataPayload=action.payload

            state.ratesData={...state.ratesData, [ratesDataPayload.name]:ratesDataPayload.value}


        },
        setRatesDataEmpty(state,action){

            state.ratesData={from:'',to:'',price:0}


        },
        setRatesDataReview(state,action){

            const dataReviewPayload=action.payload;

         

           if (Array.isArray(dataReviewPayload.result)){
             

            dataReviewPayload.result.map(elem=>{
                state.ratesDataReview.push(elem)
            })
           
                state.loaded=true
            }else{

                state.ratesDataReview.push(dataReviewPayload.result)
            }


        },
        setRatesDeleted(state,action){

            const ratesDataDeleteId=action.payload

          

            state.ratesDataReview=state.ratesDataReview.filter(ele=>ele._id!==ratesDataDeleteId)


        },
        setRatesLogout(state,action){

          

            state.loaded=false
            state.ratesDataReview=[]
           
        },
        setErrorRate(state,action){

            const errorPayload=action.payload

            state.error=errorPayload.error


        },
        setErrorEmpty(state,action){
            state.error=''
        }
    }
})

export const {setErrorRate, setErrorEmpty, setRatesData,setRatesDataEmpty,setRatesDataReview, setRatesDeleted, setRatesLogout}=ratesSlice.actions

export default ratesSlice.reducer;