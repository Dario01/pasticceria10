import {createSlice} from '@reduxjs/toolkit';




const configurationSlice=createSlice({
    name:'configurationSlice',
    initialState:{

        configurationData:{name:'',description:'',value:''},
        configurationDataResult:[],
        configurationDataUpdate:[],
        configurationCopyData:[]

    },
    reducers:{

        setConfigurationCopyData(state,action){

                state.configurationCopyData=state.configurationDataResult
        },

        setConfigurationData(state,action){

            const configurationPayload=action.payload
           

            state.configurationData={...state.configurationData, [configurationPayload.name]:configurationPayload.value}



        },

        setConfigurationDataResult(state,action){

            const configurationResultPayload=action.payload

          

          

            state.configurationDataResult.push({id:configurationResultPayload._id, name:configurationResultPayload.name, 
                description:configurationResultPayload.description, 
                value:configurationResultPayload.value, 
               enableInfo:false})

               


        },
            setChangeModifyEnable(state,action){

                const changePayload=action.payload;

              

                state.configurationDataResult.map(ele=>{
                    if(ele.name===changePayload.name){
                        ele.enableInfo=true
                        state.configurationDataUpdate.push({id:changePayload.id})
                    }

                    return ele
                })


            },

            setChangeModifyDisableAll(state,action){

                    state.configurationDataResult.map(ele=>{
                        if(ele.enableInfo=true){
                            ele.enableInfo=false
                        }

                        return ele
                    })

                
            },

            setChangeUpdateReset(state,action){


                state.configurationDataUpdate=[]
            },

            setConfigurationDataReset(state,action){

                state.configurationData={name:'',description:'',value:''}


            },


            setChangeModifyDisable(state,action){



                const changePayload=action.payload;

                state.configurationDataResult=state.configurationDataResult.map(ele=>{


                    if(ele.name===changePayload.name){


                        state.configurationCopyData.map(elem=>{

                            if(elem.name===ele.name){

                             
                                ele.enableInfo=elem.enableInfo
                                ele.name=elem.name
                                ele.description=elem.description
                                ele.value=elem.value 

                                state.configurationDataUpdate=state.configurationDataUpdate.filter(el=>el.id!==changePayload.id)

                            }
                            return elem
                        })

                        

                    }
                    return ele
                })



            },
            setGetConfigurationDataResult(state,action){

                state.configurationDataResult=[]

                const configurationGetPayload=action.payload;

               

                configurationGetPayload.map(el=>{
                    state.configurationDataResult.push({
                        id:el._id,
                        name:el.name,
                        description:el.description,
                        value:el.value,
                        enableInfo:false
                    })

                    return el
                })



            },
            setConfigurationDataUpdate(state,action){

                const updatePayload=action.payload;

               

                state.configurationDataUpdate=state.configurationDataUpdate.map(ele=>{
                    if(ele.id===updatePayload.id){
                      return  {...ele, [updatePayload.name]:updatePayload.value}
                    }
                    return ele
                })

            },

            setConfigurationDataUpdateResult(state,action){


                const changeUpdatePayload=action.payload
               

                state.configurationDataResult=state.configurationDataResult.map(ele=>{

                    if(ele.id===changeUpdatePayload.id){

                        return {...ele, [changeUpdatePayload.name]:changeUpdatePayload.value}
                    }

                    return ele


                })


            }


    }
})

export const {setConfigurationData,setConfigurationDataReset,setChangeUpdateReset,setChangeModifyDisableAll,setConfigurationCopyData,setConfigurationDataUpdateResult,setChangeModifyDisable,setConfigurationDataResult, setChangeModifyEnable, setGetConfigurationDataResult, setConfigurationDataUpdate}=configurationSlice.actions

export default configurationSlice.reducer