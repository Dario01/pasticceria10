import {createSlice} from '@reduxjs/toolkit'



const companySlice=createSlice({
    name:'companySlice',
    initialState:{
       dataCompany:{address:'',
       state:'',
       postalCode:'',
       city:'',
       country:''
    } 

    },
    reducers:{
        setDataCompany(state,action){
            const dataCompanyPayload=action.payload
                state.dataCompany={...state.dataCompany, [dataCompanyPayload.name]:dataCompanyPayload.value}

        },

        setDataCompanyLogin(state,action){

            const dataCompanyLoginPayload=action.payload.addressCompany;

           

            state.dataCompany={address:dataCompanyLoginPayload.addressStreet,
                                postalCode:dataCompanyLoginPayload.postalCode,
                                state:dataCompanyLoginPayload.state,
                                city:dataCompanyLoginPayload.city,
                                country:dataCompanyLoginPayload.country,
                                latitude:dataCompanyLoginPayload.latitude,
                                longitude:dataCompanyLoginPayload.longitude,
                                set:false}
        },

        setConfirmDataCompany(state,action){
       

            state.dataCompany.set=false
        },
        setDataClickSet(state,action){

         

            state.dataCompany.set=!state.dataCompany.set
        }

    }
})

export const {setDataCompany, setDataCompanyLogin, setConfirmDataCompany, setDataClickSet}=companySlice.actions;


export default companySlice.reducer;