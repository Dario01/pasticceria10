import {createSlice} from '@reduxjs/toolkit';


console.log('IN SLICE AUTH')

const authSlice=createSlice({

    

    name:'authSlice',
    initialState:{
        userInfo:{
        email:'',
        name:'',
        surname:'',
        state:'',
        city:'',
        country:'',
        postalCode:'',
        deliveryAddress:'',
        mainAddress:'',
        deliveryRate:'',
        set:false
        },
        token:undefined,
        errorsAuth:undefined,
        errorsReg:undefined
    },
    reducers:{
        setToken(state,action){
            const tokenPayload=action.payload
          
            return {
                ...state, token:tokenPayload
            }

        },
        setUserInfo(state,action){
           
            const userInfoPayload=action.payload.user;
            const userRatePayload=action.payload.rate

          

            {userInfoPayload.groupUser==='User' ?

            state.userInfo={
                id:userInfoPayload._id,
                email:userInfoPayload.email,
                name:userInfoPayload.name,
                surname:userInfoPayload.surname,
                groupUser:userInfoPayload.groupUser,
                state:userInfoPayload.addresses.state,
                city:userInfoPayload.addresses.city,
                country:userInfoPayload.addresses.country,
                postalCode:userInfoPayload.addresses.postalCode,
                deliveryAddress:userInfoPayload.addresses.deliveryAddress,
                mainAddress:userInfoPayload.addresses.mainAddress,
                latitude:userInfoPayload.addresses.latitude,
                longitude:userInfoPayload.addresses.longitude,
                distanceFromShop:userInfoPayload.distanceFromShop,

                
                
                deliveryRate:userRatePayload.price,
           
                set:false
            }
                :
                state.userInfo={
                    id:userInfoPayload._id,
                    email:userInfoPayload.email,
                    name:userInfoPayload.name,
                    surname:userInfoPayload.surname,
                    groupUser:userInfoPayload.groupUser,
                    state:userInfoPayload.addresses.state,
                    city:userInfoPayload.addresses.city,
                    country:userInfoPayload.addresses.country,
                    postalCode:userInfoPayload.addresses.postalCode,
                    deliveryAddress:userInfoPayload.addresses.deliveryAddress,
                    mainAddress:userInfoPayload.addresses.mainAddress,
                    latitude:userInfoPayload.addresses.latitude,
                    longitude:userInfoPayload.addresses.longitude,
                    distanceFromShop:userInfoPayload.distanceFromShop,
                    set:false

            }
            }


        },
        setErrorAuth(state,action){
            const errorAuthPayload=action.payload
            state.errorsAuth=errorAuthPayload.error
            
        },
        setErrorRegister(state,action){

            const errorRegisterPayload=action.payload;
            return {
                ...state, errorsReg:errorRegisterPayload.error
            }
        },
        setErrorAuthEmpty(state,action){
            const errorAuthEmptyPayload=action.payload;
            return {
                ...state, errorsAuth:errorAuthEmptyPayload.error
            }
        },
        setErrorRegisterEmpty(state,action){
            const errorRegisterPayload=action.payload;
            return {
                ...state, errorsReg:errorRegisterPayload.error
            }
        },
        setEnableModify(state,action){

            state.userInfo.set=!state.userInfo.set
        },
        setUserInfoChange(state,action){
            const userInfoChangePayload=action.payload

            

            state.userInfo={...state.userInfo, [userInfoChangePayload.name]:userInfoChangePayload.value}


        },
        setEnableModifyConfirmed(state,action){
            

            state.userInfo.set=false

       
        },
        setUserInfoLogout(state,action){

            const userInfoLogoutPayload=action.payload;
            state.userInfo=userInfoLogoutPayload
        }


    }
})

export const {setToken,setUserInfoChange,setUserInfoLogout, setEnableModifyConfirmed,setUserInfo, setErrorAuth, setErrorRegister,setErrorAuthEmpty,setErrorRegisterEmpty, setEnableModify}=authSlice.actions;


export default authSlice.reducer