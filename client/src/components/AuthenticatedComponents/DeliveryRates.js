import React from 'react';
import {useSelector,useDispatch} from 'react-redux';
import {selectRates,changeDataRates,insertDataRates, selectRatesReview, selectErrorRate} from '../redux/actions/indexRates';
import DeliveryRatesReview from './DeliveryRatesReview'


function DeliveryRates(){

    const rates=useSelector(selectRates)
    const review=useSelector(selectRatesReview)
    const error=useSelector(selectErrorRate)
    const dispatch=useDispatch()

 


    function changeData(e){

        dispatch(changeDataRates(e.target))

    }

    function insertDataAction(){



            dispatch(insertDataRates(rates))
    }

    return(
        <div>
        <div>
            <h3>Insert Delivery Rates</h3>
            <input type="text" name="from" placeholder="Distance From" onChange={changeData} value={rates.from}></input>
            <input type="text" name="to" placeholder="Distance To" onChange={changeData} value={rates.to}></input>
            <input type="text" name="price" placeholder="Price" onChange={changeData} value={rates.price}></input>
            <button onClick={insertDataAction}>Insert rate</button>
            {error!=='' ?
                <p>{error}</p>
                :
                null
    }

        </div>
        <div>
            <DeliveryRatesReview/>
        </div>
        </div>
    )
}

export default DeliveryRates