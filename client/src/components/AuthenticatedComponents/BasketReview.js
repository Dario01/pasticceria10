import React from 'react';
import {useSelector, useDispatch} from 'react-redux'
import {removeFromBasketReview, selectBasket, selectTotalAmount} from '../redux/actions/indexBasket';
import {selectUserInfo} from '../redux/actions/index'
import {selectCompany} from '../redux/actions/indexCompany'

import Payments from '../Access/Payments'
import MapsGoogle from './MapsGoogle'


function BasketReview(){

    const basket=useSelector(selectBasket)
    const totalAmount=useSelector(selectTotalAmount)
    const company=useSelector(selectCompany)
    const userInfo=useSelector(selectUserInfo)
    const dispatch=useDispatch()




    function removeFromBasketAction(elem,choice,totalprice,qty){
        
        dispatch(removeFromBasketReview(elem,choice,totalprice,qty))
    }

    function renderBasket(){
        return basket.map(elem=>{
            return(
                <div key={elem.name}>
                    
                        <li><b>{elem.name} </b></li><span><b>Qty:</b> {elem.qty} </span><span><b>Total Price:</b> {elem.totalprice} </span><span><b>Choice:</b> {elem.choice}</span>
            
                    <button onClick={()=>removeFromBasketAction(elem.name,elem.choice,elem.totalprice,elem.qty)}>Remove from Basket</button>
                    
                </div>
            )
        })
    }

   



    return(
        
        <div className="container"><br/>

            <h3 className="text-center">This is BasketReview</h3>
            <ol>
        {renderBasket()}
        <div>

            <Payments totalAmount={totalAmount + userInfo.deliveryRate} basket={basket}/>
        
        
        </div>
        <div>
            <p>Total Amount : {totalAmount} Euro + Delivery rate {userInfo.deliveryRate}</p>
        </div>
        </ol>
        <div>

            
        </div>

        
        </div>
    )
}

export default BasketReview;