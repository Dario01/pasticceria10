import React from 'react';
import {useSelector, useDispatch} from 'react-redux'
import { selectUserInfo } from '../redux/actions';
import {addToBasket,removeFromBasket,selectHome,selectBasket} from '../redux/actions/indexBasket'
import {autoReadingSalesHome,setDisableInfo} from '../redux/actions/indexSales';
import userPage from './userPage.css'

function UserPage(){

    const home=useSelector(selectHome)
    const basket=useSelector(selectBasket)
    
    const dispatch=useDispatch()
        
     



    function addToBasketAction(elem,price){
 

        if(price.first!==undefined && price.availableQty>0){

            const {first}=price

          return  dispatch(addToBasket({name:elem,totalprice:first,qty:1,choice:1}))


        }

        if(price.second!==undefined && price.availableQty>0){
            const {second}=price

          return  dispatch(addToBasket({name:elem,totalprice:second,qty:1,choice:2}))

        }
        
        if(price.third!==undefined && price.availableQty>0){
            const {third}=price

           return dispatch(addToBasket({name:elem,totalprice:third,qty:1, choice:3}))

        }
        
    
    }

    function removeFromBasketAction(elem,price){

      

        if(price.first!==undefined && price.qty<price.initialQty){

            

            const {first}=price

          return  dispatch(removeFromBasket({name:elem,totalprice:first,qty:1,choice:1}))


        }

        if(price.second!==undefined && price.qty<price.initialQty){

           

            const {second}=price

          return  dispatch(removeFromBasket({name:elem,totalprice:second,qty:1,choice:2}))

        }
        
        if(price.third!==undefined && price.qty<price.initialQty){

           

            const {third}=price

           return dispatch(removeFromBasket({name:elem,totalprice:third,qty:1, choice:3}))

        }


    }

    function changeDisableAction(e){
        console.log(e.target.alt)
        dispatch(setDisableInfo(e.target.alt))
    }

    function renderSales(){
        return home.map(elem=>{

           
            return(
                <div className="element-sales" key={elem.cakeId}>
                    <img src={elem.imgLink}
                    width="150"
                    height="150"
                    alt={elem.name}
                    onClick={(e)=>changeDisableAction(e)}/><br/>
                    <span className="elem-name"><b>{elem.name}</b></span>
                    <span className="elem-weight">{elem.weight}</span><br/>
                    {elem.disableInfo ? 
                            <div>
                            <ul>
                        {elem.first!==undefined ?
                        <div>
                        <li key={elem.first.price}>{elem.first.price} Euro<p>Qty: {elem.first.qty}</p></li><button onClick={()=>addToBasketAction(elem.name,{first:elem.first.price, availableQty:elem.first.qty})}>Add To Basket</button>
                        <button onClick={()=>removeFromBasketAction(elem.name,{first:elem.first.price, qty:elem.first.qty, initialQty:elem.first.initialQty})}>Remove From Basket</button>
                        </div>
                            :
                            null}
                        {elem.second!==undefined ?
                        <div>
                        <li key={elem.second.price}>{elem.second.price} Euro<p>Qty: {elem.second.qty}</p></li><button onClick={()=>addToBasketAction(elem.name,{second:elem.second.price, availableQty:elem.second.qty})}>Add To Basket</button>
                        <button onClick={()=>removeFromBasketAction(elem.name,{second:elem.second.price, qty:elem.second.qty, initialQty:elem.second.initialQty})}>Remove From Basket</button>
                        </div>
                        :
                        null
                        }
                        {elem.third!==undefined ?
                        <div> 
                        <li key={elem.third.price}>{elem.third.price} Euro<p>Qty: {elem.third.qty}</p></li><button onClick={()=>addToBasketAction(elem.name,{third:elem.third.price, availableQty:elem.third.qty})}>Add To Basket</button>
                        <button onClick={()=>removeFromBasketAction(elem.name,{third:elem.third.price, qty:elem.third.qty, initialQty:elem.third.initialQty})}>Remove From Basket</button>
                        </div>
                                :
                                null
                        }
                            </ul>
                            <span>Ingredients</span>
                            <ul>
                                {elem.ingredients.map(ingredient=>{
                                    return (
                                        <li key={ingredient}>{ingredient}</li>
                                    )
                                })}
                            </ul>
                            </div>
                            
                         :
                         null   
                        }
                        
                </div>
            )
        })
    }

    return(

        <div className="user-page">
            <h3 className="title">This is userpage</h3>
            <div className="cakes">
                {renderSales()}
            </div>

        </div>
    )
}

export default UserPage;