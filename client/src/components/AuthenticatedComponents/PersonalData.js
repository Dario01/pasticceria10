import React from 'react';
import {useDispatch,useSelector} from 'react-redux'
import {confirmUserData, selectPersonalData, changeDataPersonal} from '../redux/actions/indexPersonalData';
import {selectUserInfo,enableModify} from '../redux/actions/index'


function PersonalData(){

    const dispatch=useDispatch()
    const personalData=useSelector(selectPersonalData)
    const userInfo=useSelector(selectUserInfo)






    function handleSubmit(e){

        e.preventDefault()
        dispatch(confirmUserData(userInfo))

    }

    function changeData(e){

        dispatch(changeDataPersonal(e.target))
    }

    function setEnableModifyAction(){


        dispatch(enableModify())
    }

    return(
        <div>
            <h3>This is personal data page</h3>
{userInfo.set===true ?
            <form onSubmit={handleSubmit}>
    <label>Email</label><br/>
    <input type="text" value={userInfo.email} name="email" disabled></input><br/>
    <label>Name</label><br/>
    <input type="text" name="name" value={userInfo.name} onChange={changeData}></input><br/>
    <label>Surname</label><br/>
    <input type="text" name="surname" value={userInfo.surname} onChange={changeData}></input><br/>
    <label>State</label><br/>
    <input type="text" name="state" value={userInfo.state} onChange={changeData}></input><br/>
    <label >City</label><br/>
    <input type="text" name="city" value={userInfo.city} onChange={changeData}></input><br/>
    <label >Country</label><br/>
    <input type="text" name="country" value={userInfo.country} onChange={changeData}></input><br/>
    <label >Postal Code</label><br/>
    <input type="text" name="postalCode" value={userInfo.postalCode} onChange={changeData}></input><br/>
    <label >Delivery Address</label><br/>
    <input type="text" name="deliveryAddress" value={userInfo.deliveryAddress} onChange={changeData}></input><br/>
    <label >Main Address</label><br/>
    <input type="text" name="mainAddress" value={userInfo.mainAddress} onChange={changeData}></input><br/>
        <button type="submit">Save</button>
            </form>
:
<div>
<label>Email</label><br/>
<input type="text" value={userInfo.email} name="email" disabled></input><br/>
<label>Name</label><br/>
<input type="text" name="name" value={userInfo.name} disabled></input><br/>
<label>Surname</label><br/>
<input type="text" name="surname" value={userInfo.surname} disabled></input><br/>
<label>State</label><br/>
<input type="text" name="state" value={userInfo.state} disabled></input><br/>
<label >City</label><br/>
<input type="text" name="city" value={userInfo.city} disabled></input><br/>
<label >Country</label><br/>
<input type="text" name="country" value={userInfo.country} disabled></input><br/>
<label >Postal Code</label><br/>
<input type="text" name="postalCode" value={userInfo.postalCode} disabled></input><br/>
<label >Delivery Address</label><br/>
<input type="text" name="deliveryAddress" value={userInfo.deliveryAddress} disabled></input><br/>
<label >Main Address</label><br/>
<input type="text" name="mainAddress" value={userInfo.mainAddress} disabled></input><br/>
<button onClick={setEnableModifyAction}>Enable Modify</button>
        </div>

}

</div>


    )
}

export default PersonalData