import React, {useEffect} from 'react';
import {selectRatesReview, deleteRate, autoLoadRates, selectRatesLoading} from '../redux/actions/indexRates';
import {useSelector,useDispatch} from 'react-redux';


function DeliveryRatesReview(){


  

    const ratesReview=useSelector(selectRatesReview)
    const loaded=useSelector(selectRatesLoading)
    const dispatch=useDispatch()

    useEffect(()=>{

        if(loaded===false){

       
        dispatch(autoLoadRates())
        }

    }, [])

 

    function deleteRateAction(e){

       

        dispatch(deleteRate(e.target.id))

    }

    function ratesReviewOrder(){
        return ratesReview.map(rate=>{
            return(
                <div key={rate._id}>
                    <ul>
                        <li>{rate.distanceFrom} - {rate.distanceTo} Price : {rate.price}</li>
                    </ul>
                    <button id={rate._id} onClick={deleteRateAction}>Delete</button>
                </div>
            )
        })
    }


    return(
        <div>
            <h5>This is your rates delivery</h5>
            <div>
                {ratesReviewOrder()}
            </div>

        </div>
    )
}

export default DeliveryRatesReview;