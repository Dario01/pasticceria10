import React, {useEffect} from 'react';
import {useSelector,useDispatch} from 'react-redux';
import {selectCompany,changeDataCompany, confirmDataCompany,setDataClick, autoDataCompany} from '../redux/actions/indexCompany';



function CompanyData(){

    const company=useSelector(selectCompany)
    const dispatch=useDispatch()


    // useEffect(()=>{

    //     dispatch(autoDataCompany())
    // },[])


    
    function handleSubmit(e){

        e.preventDefault()

        dispatch(confirmDataCompany(company))

    }

    function changeData(e){

       

        dispatch(changeDataCompany(e.target))

    }

    function setDataClickAction(){

        dispatch(setDataClick())
    }


    return(
        <div>
            <h3>
                Insert the address of your company
            </h3>
            <div>
            {company.set===true ?
            <form onSubmit={handleSubmit}>
           
            
                <input type="text" value={company.address} placeholder="address" onChange={changeData} name="address"></input><br/>
                <input type="text" value={company.country} placeholder="country" onChange={changeData} name="country"></input><br/>
                <input type="text" value={company.state} placeholder="state" onChange={changeData} name="state"></input><br/>
                <input type="number" value={company.postalCode} placeholder="postalCode" onChange={changeData} name="postalCode"></input><br/>
                <input type="text" value={company.city} placeholder="city" onChange={changeData} name="city"></input><br/>
                <button type="submit">Save</button>
                
                </form>
                :
                <div>
                
                <input type="text" value={company.address} placeholder="address" onChange={changeData} name="address" disabled></input><br/>
                <input type="text" value={company.country} placeholder="country" onChange={changeData} name="country" disabled></input><br/>
                <input type="text" value={company.state} placeholder="state" onChange={changeData} name="state" disabled></input><br/>
                <input type="number" value={company.postalCode} placeholder="postalCode" onChange={changeData} name="postalCode" disabled></input><br/>
                <input type="text" value={company.city} placeholder="city" onChange={changeData} name="city" disabled></input><br/>

                <button onClick={setDataClickAction}>Enable Modify</button>
                </div>
                }
            
                
            
            </div>

        </div>
    )
}

export default CompanyData