import React from 'react';
import {Link} from 'react-router-dom'


function SuccessPayment(){

    return(
        <div>
            <h3>Congratulations</h3>
            <h5>Your payments has been accepted</h5>
            <p>You will receive your cake tomorrow before the 8 pm, click here to come back in your home page <Link to="/userPage">Home</Link></p>


        </div>
    )
}

export default SuccessPayment;