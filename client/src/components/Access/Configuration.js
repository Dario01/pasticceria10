import React, {useEffect} from 'react';
import {useDispatch,useSelector} from 'react-redux';
import {changeDataConfiguration, changeConfiguration, selectConfiguration, sendConfiguration, changeModifyEnable,changeModifyDisable, autoReadingConfiguration, sendModifyData} from '../redux/actions/indexConf';



function Configuration(){

    const dispatch=useDispatch()
    const configurationData=useSelector(selectConfiguration)



    function changeDataConfigurationAction(e){

      
        dispatch(changeDataConfiguration(e.target))

    }

    function sendConfigurationAction(){

       

        if(configurationData.configurationData.name===''){

            

            return false
        }

        
           dispatch(sendConfiguration(configurationData))
    }

    function changeModifyActionEnableAction(e){


        dispatch(changeModifyEnable(e.target))


    }

    function changeModifyActionDisableAction(e){

        dispatch(changeModifyDisable(e.target))

    }

    useEffect(()=>{

        dispatch(autoReadingConfiguration())


    }, [])

    function changeConfigurationAction(e){

        dispatch(changeConfiguration(e.target))


    }


    function sendModifyDataAction(){

        if(configurationData.configurationDataUpdate.length===0){

         

            return false
        }

        dispatch(sendModifyData(configurationData))
    }




    function renderConfiguration(){

        return configurationData.configurationDataResult.map(el=>{

            

            return (
                
                
                
                <div key={el.name}>
               {el.enableInfo===false ?
               <div >
                <input type="text" value={el.name} disabled></input>
                <input type="text" value={el.description} disabled></input>
                <input type="text" value={el.value} disabled></input>
                <button id={el.id} name ={el.name}onClick={changeModifyActionEnableAction}>Enable modify</button>
                </div>
                :
                <div >
                <input id={el.id} name="name" type="text" value={el.name}  onChange={changeConfigurationAction} ></input>
                <input id={el.id} name="description" type="text" value={el.description} onChange={changeConfigurationAction} ></input>
                <input id={el.id} name="value" type="text" value={el.value}onChange={changeConfigurationAction} ></input>
                <button id={el.id} name = {el.name} onClick={changeModifyActionDisableAction}>Disable modify</button>
                </div>

               }
                </div>

          
                
                
            )
        })
    }




    return (
        <div>
            <h3>Insert Configuration</h3>
            <div>
                <input type="text" name="name" onChange={changeDataConfigurationAction} value={configurationData.configurationData.name}></input>
                <input type="text" name="description" onChange={changeDataConfigurationAction} value={configurationData.configurationData.description}></input>
                <input type="text" name="value" onChange={changeDataConfigurationAction} value={configurationData.configurationData.value}></input>
            </div>

            <button onClick={sendConfigurationAction}>Send configuration</button>
<div>
    
        {renderConfiguration()}
    
</div>
<button onClick={sendModifyDataAction}>Send modified Data</button>
        
        </div>
    )
}

export default Configuration;