import React from 'react';
import StripeCheckOut from 'react-stripe-checkout';
import {useDispatch,useSelector} from 'react-redux'
import {handlePayments} from '../redux/actions/indexBasket';
import {selectUserInfo} from '../redux/actions/index';
import {withRouter} from 'react-router-dom'


function Payments(props){

    const dispatch=useDispatch()
    const apiKey='pk_test_nJ0tQca0WIDt7gbEdK7MIJqS002QdmteXb';

    const userInfo=useSelector(selectUserInfo)

   

   function handlePaymentsAction(token){

   
        dispatch(handlePayments(token,props.totalAmount,props.basket,props.history))

    }


    return(
        <div>
            <StripeCheckOut
            email={userInfo.email}
            name="Cakkes"
            description="buy this cake"
            token={token=>handlePaymentsAction(token)}
            stripeKey={apiKey}>

<button >Proceed to order</button>

            </StripeCheckOut>

        </div>

    )
}

export default withRouter(Payments)