import React from 'react';
import {Link} from 'react-router-dom'



function PersonalDataIcon(){

    return(
        <div>
            
            <Link to="/personalData"><i className="fas fa-user"></i></Link>
        </div>
    )
}

export default PersonalDataIcon;