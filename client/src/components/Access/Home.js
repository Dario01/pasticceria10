import React, { useEffect } from 'react';
import * as actions from '../redux/actions/indexSales';
import {connect} from 'react-redux';
import './home.css';

//**REDUX TOOLKIT */
import {useSelector,useDispatch} from 'react-redux';

import {selectHome} from '../redux/actions/indexBasket'
import {setDisableInfo,autoReadingSalesHome} from '../redux/actions/indexSales'



//** */



function Home(){

    //**REDUX TOOLKIT */

    const dispatch=useDispatch()

    const home=useSelector(selectHome)

  

    //** */





    useEffect(()=>{

       // props.autoReadingSalesHome()

        //**REDUX TOOLKIT */

        dispatch(autoReadingSalesHome())

        //** */

    },[])

function changeDisableAction(e){
    console.log(e.target.alt)
    dispatch(setDisableInfo(e.target.alt))
}



    // function renderSales(){
    //     return props.home.map(elem=>{
    //         return(
    //             <div className="element-sales" key={elem.cakeId}>
    //                     <img src={elem.listcakes[0].imgLink}
    //                     width="150"
    //                     height="150"
    //                     alt={elem.listcakes[0].name}
    //                     onClick={(e)=>changeDisable(e)}
    //                     /><br/>
    //                     <span className="elem-name"><b>{elem.listcakes[0].name}</b>
    //                     </span><span className="elem-weight">{elem.listcakes[0].weight}</span><br/>
                        
    //                     {elem.disableInfo ? 
    //                         <div>
    //                         <ul>
    //                             <li key={elem.first}>{elem.first} Euro</li>
    //                             <li key={elem.second}>{elem.second} Euro</li>
    //                             <li key={elem.third}>{elem.third} Euro</li>
    //                         </ul>
    //                         <span>Ingredients</span>
    //                         <ul>
    //                             {elem.listcakes[0].ingredients.map(ingredient=>{
    //                                 return (
    //                                     <li key={ingredient}>{ingredient}</li>
    //                                 )
    //                             })}
    //                         </ul>
    //                         </div>
                            
    //                      :
    //                      null   
    //                     }
    //                     </div>
                        
                
    //         )
    //     })


    // }

    //**REDUX TOOLKIT */

    function renderSales(){
        return home.map(elem=>{
            
            return(
                
                <div className="col-12 col-md-4"  key={elem.cakeId}>
                    
                        <img src={elem.imgLink}
                        width="150"
                        height="150"
                        alt={elem.name}
                        onClick={(e)=>changeDisableAction(e)}
                        /><br/>
                        <span className="elem-name"><b>{elem.name}</b>
                        </span><span className="elem-weight">{elem.weight}</span><br/>
                        
                        {elem.disableInfo ? 
                        
                            <div>
                            <ul>
                                {elem.first!==undefined ?
                                <div><li >{elem.first.price} Euro</li><p>Qty {elem.first.qty}</p></div>
                                :
                                null}
                                
                                {elem.second!==undefined ?
                                <div><li >{elem.second.price} Euro</li><p>Qty {elem.second.qty}</p></div>
                                :
                                null}
                                {elem.third!==undefined ?
                                <div><li >{elem.third.price} Euro</li><p>Qty {elem.third.qty}</p></div>
                                    :
                                    null
                            }
                   
                               
                            </ul>
                            <span>Ingredients</span>
                            <ul>
                                {elem.ingredients.map(ingredient=>{
                                    return (
                                        <li key={ingredient}>{ingredient}</li>
                                    )
                                })}
                            </ul>
                            </div>
                            
                         :
                         null   
                        }
                        </div>
                        
                        
                
            )
        })


    }

  //** */


    return(
        
        <div className="container">
            <h1 className="text-center">This is home</h1>
            
            <div className="row">
            {renderSales()}
            </div>
            </div>
            
        
    )
}

// function mapStateToProps(state){
//     console.log('state home',state)
//     return{
//         home:state.home.home
//     }
// }



//export default connect(mapStateToProps,actions)(Home);


//**REDUX TOOLKIT */

export default Home;

//** */