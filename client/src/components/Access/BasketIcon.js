import React from 'react';
import {useSelector} from 'react-redux';
import {selectTotal} from '../redux/actions/indexBasket';
import {Link} from 'react-router-dom'




function BasketIcon(){

    const total=useSelector(selectTotal)



    return(
        <div className="shopping-cart">
        <Link to="/basketReview"><i className="fas fa-shopping-cart"><sup>{total}</sup></i></Link>
        </div>
    )
}

export default BasketIcon;