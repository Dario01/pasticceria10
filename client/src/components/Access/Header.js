import React from 'react';
import {connect} from 'react-redux';
import {NavLink, withRouter, Link} from 'react-router-dom'
import BasketIcon from './BasketIcon';
import PersonalDataIcon from './PersonalDataIcon'
import basketIcon from './basketIcon.css'


// function Header(props){


//     function renderHeader(){
//         switch (props.auth.token){
//             case undefined:
//                 return (
//                     <div>
//             <ul className="nav">
//             <li className="nav-item">
//             {/* <a className="nav-link active" href="/register">Register</a> */}
//             <NavLink activeClassName="text-info" to="/login" className="nav-link">Login</NavLink>
//             </li>
//             <li className="nav-item">
//             {/* <a className="nav-link active" href="/login">Login</a> */}
//             <NavLink activeClassName="text-info" to="/register" className="nav-link">Register</NavLink>
//             </li>
//             </ul>
//         </div>
//                 )
//                 default:
//                     return (
//                         <div>
//                         <ul className="nav">
//                         <li className="nav-item">
//                         <a className="nav-link active" onClick={props.logout}>Logout</a>
//                         </li>
//                         </ul>
//                         </div>

//                     )
//         }
//     }




//     console.log(props)

//     return(
//         <div>
//             {renderHeader()}
//         </div>


//     )
// }

// function mapStateToProps(state){
//     return{
//         auth:state.auth
//     }
// }

// export default connect(mapStateToProps,null)(Header);

function Header(props){


    function renderHeader(){
        switch (props.token){
            case undefined:
                return (
                    <div>
            <ul className="nav">
            <li className="nav-item">
            {/* <a className="nav-link active" href="/register">Register</a> */}
            <NavLink activeClassName="text-info" to="/login" className="nav-link">Login</NavLink>
            </li>
            <li className="nav-item">
            {/* <a className="nav-link active" href="/login">Login</a> */}
            <NavLink activeClassName="text-info" to="/register" className="nav-link">Register</NavLink>
            </li>
            </ul>
        </div>
                )
                default:
                    return (
                        <div>
                            <div className="authentication-link">
                        <ul className="nav">
                        <li className="nav-item">
                        <a className="nav-link active" onClick={props.logout}>Logout</a>
                        
                        </li>
                        </ul>
                        </div>
                        <div>
                        
                        </div>
                        { props.userInfo.groupUser==='User' ?
                        <div>
                        <BasketIcon/>
                        <PersonalDataIcon/>
                        </div>
                        
                        :
                        <div>
                        <Link to="/companyData">Set data</Link>
                        <Link to="/deliveryRates">Set delivery Rates</Link>
                        <Link to="/configuration">Configuration</Link>
                        </div>
                        }
                        </div>

                    )
        }
    }




    console.log(props)

    return(
        <div>
            {renderHeader()}
        </div>


    )
}

export default Header;