import React,{useState, useEffect} from 'react';
import * as actions from '../redux/actions/index';
import {connect} from 'react-redux';
import {withRouter, Route} from 'react-router-dom';
import RegisterSuccess from './RegisterSuccess';
import {useDispatch,useSelector} from 'react-redux'
import {selectErrorReg,
        register,
        registerEmpty} from '../redux/actions/index'


// function Register(props){

//     const dispatch=useDispatch()
//     const error=useSelector(selectError)


//     const [userData,setUserData]=useState({email:'',password:''})

//     function changeData(e){
//         setUserData({...userData, [e.target.name]:e.target.value})

//     }

//     function registerUser(e){
//         e.preventDefault()
//         dispatch(register(userData,props.history))
//     }



//     return(

//         <div>
            
//         <form onSubmit={registerUser}>
//             <h1>Registrati</h1>
//             <div className="form-group">
//     <label>Email address</label>
//     <input type="email" className="form-control" name="email" placeholder="Email"
//     value={userData.email} onChange={changeData}/>
        
//     </div>
//     <div className="form-group">
//     <label >Password</label>
//     <input type="password" className="form-control" name="password" placeholder="Password"
//     value={userData.password} onChange={changeData}/>
        
//   </div>
  
//   <button type="submit" className="btn btn-primary" >Submit</button>
  
// </form>
// <div>

// <p>{ props.error?.error }</p>

    


// </div>
// </div>


//     )
// }

// function mapStateToProps(state){
//     return{
//         error:state.auth.errors
//     }
// }


// export default connect(mapStateToProps,actions)(withRouter(Register));


function Register(props){

    const dispatch=useDispatch()
    const error=useSelector(selectErrorReg)

    useEffect(()=>{

        dispatch(registerEmpty())
    
    
    }, [])


    const [userData,setUserData]=useState({email:'',password:'',name:'',surname:'',city:'',country:'',postalCode:'',
                                            state:'',deliveryAddress:'',mainAddress:''})

    function changeData(e){
        setUserData({...userData, [e.target.name]:e.target.value})

    }

    function registerUser(e){
        e.preventDefault()
        dispatch(register(userData,props.history))
    }



    return(

        <div>
            
        <form onSubmit={registerUser}>
            <h1>Registrati</h1>
            <div className="form-group">
    <label>Email address</label>
    <input type="email" className="form-control" name="email" placeholder="Email"
    value={userData.email} onChange={changeData}/>
        
    </div>
    <div className="form-group">
    <label >Password</label>
    <input type="password" className="form-control" name="password" placeholder="Password"
    value={userData.password} onChange={changeData}/>
    <label >Name</label>
    <input type="text" className="form-control" placeholder="Name" name="name" value={userData.name} onChange={changeData}></input>
    <label >Surname</label>
    <input type="text" className="form-control" placeholder="Surname" name="surname" value={userData.surname} onChange={changeData}></input>
    <label >State</label>
    <input type="text" className="form-control" placeholder="State" name="state" value={userData.state} onChange={changeData}></input>
    <label >City</label>
    <input type="text" className="form-control" placeholder="City" name="city" value={userData.city} onChange={changeData}></input>
    <label >Country</label>
    <input type="text" className="form-control" placeholder="Country" name="country" value={userData.country} onChange={changeData}></input>
    <label >Postal Code</label>
    <input type="text" className="form-control" placeholder="Postal Code" name="postalCode" value={userData.postalCode} onChange={changeData}></input>
    <label >Delivery Address</label>
    <input type="text" className="form-control" placeholder="Delivery Address" name="deliveryAddress" value={userData.deliveryAddress} onChange={changeData}></input>
    <label >Main Address</label>
    <input type="text" className="form-control" placeholder="Main Address" name="mainAddress" value={userData.mainAddress} onChange={changeData}></input>
    
        
  </div>
  
  <button type="submit" className="btn btn-primary" >Submit</button>
  
</form>
<div>

<p>{error ? error : null}</p>

    


</div>
</div>


    )
}




export default withRouter(Register);