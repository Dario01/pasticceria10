const nodemailer=require('nodemailer');
const mg=require('nodemailer-mailgun-transport');
const emailOrderTemplate=require('./emailOrderTemplate')

module.exports=(email,basket)=>{


    const auth={
        auth:{
            domain:'sandbox40c71e1d12554d809a588a1153b61ddc.mailgun.org',
            api_key:'e1050e87d79ed8c9e8c4d9a6c50ef4de-b6190e87-fcccf9d0'
        }
    }

    const nodemailerMailGun=nodemailer.createTransport(mg(auth))

    return new Promise((resolve,reject)=>nodemailerMailGun.sendMail({
        from:'test@sandbox40c71e1d12554d809a588a1153b61ddc.mailgun.org',
        to:`${email}`,
        subject:'Your order',

        html:emailOrderTemplate(basket)
    }, (err,info)=>{

        if (err){
            return reject(err)
        }else{
            console.log('INFO EMAIL ORDER',info)
            return resolve(true)
        }

    }))


}