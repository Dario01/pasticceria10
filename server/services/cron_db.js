const cron=require('node-cron')
var MongoClient=require('mongodb').MongoClient;
const keys=require('./config')
const Stock=require('../database/model/Stock')
const Configurations=require('../database/model/Configuration')


var generalTask=cron.schedule("49 20 * * *", function(){

  console.log('STARTING SCHEDULE')


  Configurations.findOne({name:"Stock generation", value:true}).then(resi=>{





    if(resi!==null){

      var task=cron.schedule("53 20 * * *", function(){




    console.log('starting job', new Date())

    MongoClient.connect(keys.MONGODB_URI.toString(), function (err, db) {
        if (err) throw err;
  
        if(process.env.NODE_ENV==='production'){
          
          var dbo=db.db("cakkes")
  
        }else{
        var dbo = db.db("paste");
        }

        dbo
        .collection("salepricelists")
        .aggregate([
          {
            $lookup: {
              from: "cakes",
              localField: "cakeId",
              foreignField: "_id",
              as: "listcakes",
            },
          },
          {$match:{sales:true}}
          
        ])
        .toArray(function (err, resp) {
          if (err) throw err;

          

            

           
            resp.map(elem=>{
                   
              
                const stocks=new Stock({
                    nameCake:elem.listcakes[0].name,
                    qty:elem.dailyProduction,
                    choice:1,
                    price:elem.first

                })

                stocks.save().then(respe=>console.log('RES',respe))
               
            })
          
        });

})

})


var changeChoiceSecondPrice=cron.schedule("52 20 * * *", function(){

    console.log('FIRST TO SECOND JOB')

    MongoClient.connect(keys.MONGODB_URI.toString(), function (err, db) {
        if (err) throw err;
  
        if(process.env.NODE_ENV==='production'){
          
          var dbo=db.db("cakkes")
  
        }else{
        var dbo = db.db("paste");
        }

        dbo
        .collection("salepricelists")
        .aggregate([
          {
            $lookup: {
              from: "cakes",
              localField: "cakeId",
              
              foreignField: "_id",

              as: "listcakes",
            },
          },
          {$match:{sales:true}}
        ])
        .toArray(function (err, resp) {
          if (err) throw err;

         

          const nameCake=resp.map(elem=>{
              return {
                  name:elem.listcakes[0].name,
                  price:elem.second
              }
              
          })
         

          const priceCake=resp.map(elem=>{
            return (
                
               elem.second
            )
          })

          

          nameCake.map(elem=>{

            Stock.update({nameCake:elem.name, choice:1},{$set:{choice:2,price:elem.price}}).then(res=>console.log('RESSSIN',res))
        })



            // Stock.updateMany({nameCake: {$in:nameCake}, choice:1},{$set:{choice:2, price:price-5}}).then(res=>{
            //     console.log('RES IN STOCK',res)
            // })
          
})
    })

})


var changeChoiceThirdPrice=cron.schedule("51 20 * * *", function(){

    console.log('SECOND TO THIRD JOB')

    MongoClient.connect(keys.MONGODB_URI.toString(), function (err, db) {
        if (err) throw err;
  
        if(process.env.NODE_ENV==='production'){
          
          var dbo=db.db("cakkes")
  
        }else{
        var dbo = db.db("paste");
        }

        dbo
        .collection("salepricelists")
        .aggregate([
          {
            $lookup: {
              from: "cakes",
              localField: "cakeId",
              foreignField: "_id",
              as: "listcakes",
            },
          },
        ])
        .toArray(function (err, resp) {
          if (err) throw err;

         

          const nameCake=resp.map(elem=>{
              return {
                  name:elem.listcakes[0].name,
                    price:elem.third
              }

              
              
          })
          
          

          

          nameCake.map(elem=>{

            Stock.update({nameCake:elem.name, choice:2},{$set:{choice:3,price:elem.price}}).then(res=>console.log('RES',res))
        })



            // Stock.updateMany({nameCake: {$in:nameCake}, choice:1},{$set:{choice:2, price:price-5}}).then(res=>{
            //     console.log('RES IN STOCK',res)
            // })
          
})
    })

})

var deleteThirdChoice=cron.schedule("50 20 * * *",function(){

    console.log('DELETE JOB')

    Stock.deleteMany({choice:3}).then(res=>console.log('delete',res))



})

task.start()
changeChoiceSecondPrice.start()
changeChoiceThirdPrice.start()
deleteThirdChoice.start()


    }else{


      console.log('VALUE IS FALSE')
      return false
    }


})




})

generalTask.start()
