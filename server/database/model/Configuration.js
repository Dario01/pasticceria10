const mongoose=require('mongoose');
const {Schema}=mongoose;



const ConfigurationSchema=new Schema({
    name:{
        type:String
    },
    description:{
        type:String
    },
    value:{
        type:Boolean
    }
})

const Configurations=mongoose.model('configurations',ConfigurationSchema);

module.exports=Configurations;