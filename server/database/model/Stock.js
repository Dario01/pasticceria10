const mongoose=require('mongoose');
const {Schema}=mongoose;


const StockSchema=new Schema({
    nameCake:String,
    qty:Number,
    choice:Number,
    price:Number
},
{ typeKey: '$type' }
)


const Stock=mongoose.model('stocks',StockSchema);

module.exports=Stock;