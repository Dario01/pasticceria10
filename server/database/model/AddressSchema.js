const mongoose=require('mongoose');
const {Schema}=mongoose;

const AddressSchema=new Schema({
    addressStreet:{
        type:String
    },
    postalCode:{
        type:Number
    },
    city:{
        type:String
    },
    country:{
        type:String
    },
    state:{
        type:String
    },
    latitude:{
        type:Number
    },
    longitude:{
        type:Number
    },
    _id:false
})

module.exports=AddressSchema;