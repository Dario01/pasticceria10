const mongoose=require('mongoose')
const {Schema}=mongoose;

const RatesTableSchema=new Schema({
    
    distanceFrom:{
        type:Number
    },
        distanceTo:{
            type:Number
        },
        price:{
            type:Number
        }
    
})

const Rates=mongoose.model('rates', RatesTableSchema);

module.exports=Rates