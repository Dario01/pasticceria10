const mongoose=require('mongoose');
const {Schema}=mongoose;


const UserAddressSchema=new Schema({
    deliveryAddress:{
        type:String
    },
    mainAddress:{
        type:String
    },
    postalCode:{
        type:Number
    },
    city:{
        type:String
    },
    country:{
        type:String
    },
    state:{
        type:String
    },
    latitude:{
        type:Number
    },
    longitude:{
        type:Number
    },
    _id:false
})

module.exports=UserAddressSchema