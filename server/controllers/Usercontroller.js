const User = require("../database/model/User");
const Company=require("../database/model/Company")

const axios = require("axios");
const jwt = require("jwt-simple");
const config = require("../config/config");
const emailService = require("../services/emailService");
const emailSender = require("../services/emailSender");
const Rates = require("../database/model/RatesTable");


function tokenForUser(user) {
  var timestamp = new Date().getTime();
  return jwt.encode({ sub: user.id, iat: timestamp }, config.secret);
}

module.exports = {
  register(req, res) {
    if (!req.body.email || !req.body.password) {
      return res
        .status(422)
        .send({ error: "You have to provide an email and password" });
    }

    User.findOne({ email: req.body.email }, function (err, existingUser) {
      if (err) {
        return res.status(422).send({ error: err });
      }
      if (existingUser) {
        return res.status(408).send({ error: "This email already exist" });
      }

      //achieve the latitude and longitude

      console.log("REQ BODY USER", req.body);

      emailService(req.body.email)
                .then((reeee) => {
                 


                  axios
                  .get("https://maps.googleapis.com/maps/api/geocode/json", {
                    params: {
                      address: req.body.postalCode + " " + req.body.deliveryAddress,
                      key: "INSERT KEY",
                    },
                  })
                  .then((rese) => {
                    console.log("RESE IN USER", rese.data.results[0].geometry.location);

                    Company.find({}).then(resi=>{
                        console.log('RESSSSSSSSSSSSSSSSSSSSSSSSIIIII',resi)

                        if(resi.length===0){
                            console.log('INSIDE IF RESI')
                        console.log("RESE IN USER 22222", rese.data.results[0].geometry.location);
                        
                        //axios.get("https://maps.googleapis.com/maps/api/js?key=AIzaSyAX52_i8ikyQQD0fihSi0n_mOjkNU2nHCc&libraries=geometry").then(r=>{

                            const user = new User({
                                email: req.body.email,
                                password: req.body.password,
                                name:req.body.name,
                                surname:req.body.surname,
                                groupUser:
                                  req.body.email === "dario.lostumbo@hotmail.com"
                                    ? "Admin"
                                    : "User",
                                addresses: {
                                  city: req.body.city,
                                  country: req.body.country,
                                  postalCode: req.body.postalCode,
                                  state: req.body.state,
                                  deliveryAddress: req.body.deliveryAddress,
                                  mainAddress: req.body.mainAddress,
                                  latitude: rese.data.results[0].geometry.location.lat,
                                  longitude: rese.data.results[0].geometry.location.lng,
                                  
                                }
                              });
                    
                          
                                user.save().then((user) => {
                                  console.log("USERIIIID", user._id);
          
          
          
                                      emailSender(user._id, req.body.email)
          
                                      console.log('AFTER EMAIL SENDER')
          
                                   return res.status(200).send({ user: user });
                                      }).catch((error) => {
                                          console.log("ERRRRORRRRR", error);
                                          const message = error.message;
                                     return res.status(400).send({ error: message, user: user });
                                        })






                        
                        }else{
                        var latitude1=rese.data.results[0].geometry.location.lat;
                        var longitude1=rese.data.results[0].geometry.location.lng;
                        var latitude2=resi[0].addressCompany.latitude;
                        var longitude2=resi[0].addressCompany.longitude;
                        console.log('LATI2',latitude2)
                        console.log('LONGI2',longitude2)

                        
                          
                             axios.get("https://maps.googleapis.com/maps/api/distancematrix/json",{
                                 params:{
                                       
                                     origins:`${latitude1}`+ ',' + `${longitude1}`,
                                    destinations:`${latitude2}`+ ',' + `${longitude2}`,
                                     key: "INSERT KEY"

                                 }
                             }).then(reseef=>{
                                 console.log('REEEESESEEEF',reseef)
                                 console.log('REEEESESEEEF',reseef.data.rows[0].elements[0].distance.text)
                             var distance=parseFloat(reseef.data.rows[0].elements[0].distance.text)

                             console.log('DISTANCE',distance)

                            //  axios.get(`https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=${latitude1},${longitude1}&destinations=${latitude2},${longitude2}&key=KEY`)
                            //  .then(resef=>console.log('RESEEEEF',resef.data.rows[0].elements))
                        
                        
          
                    const user = new User({
                      email: req.body.email,
                      password: req.body.password,
                      name:req.body.name,
                      surname:req.body.surname,
                      groupUser:
                        req.body.email === "dario.lostumbo@hotmail.com"
                          ? "Admin"
                          : "User",
                      addresses: {
                        city: req.body.city,
                        country: req.body.country,
                        postalCode: req.body.postalCode,
                        state: req.body.state,
                        deliveryAddress: req.body.deliveryAddress,
                        mainAddress: req.body.mainAddress,
                        latitude: rese.data.results[0].geometry.location.lat,
                        longitude: rese.data.results[0].geometry.location.lng,
                        
                      },
                      distanceFromShop:distance
                    });
          
                
                      user.save().then((user) => {
                        console.log("USERIIIID", user._id);



                            emailSender(user._id, req.body.email)

                            console.log('AFTER EMAIL SENDER')

                            res.send({ user: user });
                            }).catch((error) => {
                                console.log("ERRRRORRRRR", error);
                                const message = error.message;
                                res.status(400).send({ error: message, user: user });
                              })
                            }).catch(err=>console.log('ERR FIND DISTANCE',err))
                        }
                            }).catch(err=>console.log('INSIDE BEFORE ERR IN GEOCODE',err))
                        
                            }).catch((err) => {
                                console.log("ERR IN GEOCODE USER", err);
                              })
                }).catch(() =>{
                res.status(400).send({ error: "This email is not valid" })
                })
    }).catch(() =>{
    res.status(400).send({ error: "This email is not valid" })
    })
          
    

  },

  login(req, res) {
   

    var email = req.body.email;
    var password = req.body.password;

    if (!req.body.email || !req.body.password) {
      return res
        .status(422)
        .send({ error: "Please provide an email and password" });
    }

    User.findOne({ email: email }, function (err, existingUser) {
      if (err) {
        return res.status(422).send({ error: err });
      }
      if (!existingUser) {
        return res.status(401).send({ error: "Wrong email" });
      }

      if (existingUser.active === false) {
        return res
          .status(401)
          .send({
            error:
              "Please, you have to activate your account clicking the link that we have sent to your email",
          });
      }

      existingUser.comparePassword(password, function (err, result) {
        if (err) {
          return res.status(422).send({ error: err });
        }
        if (!result) {
          return res.status(401).send({ error: "Wrong password" });
        }

       console.log('EXSISTING USER ADMIN',existingUser)
        if(existingUser.groupUser==='Admin'){

            console.log('inside existing')
        Company.find({}).then(resp=>{
            if(resp.length===0){
                console.log('inside if',resp.length)
      return  res.status(200).send({ user: existingUser, token: tokenForUser(existingUser)})
            }else{
                console.log('outside existing',existingUser)
                console.log('outside',resp)


                return  res.status(200).send({ user: existingUser, token: tokenForUser(existingUser), company:resp })
            }
    }).catch(err=>console.log('ERR',err))
}else{

    Rates.findOne({$and:[{distanceFrom:{$lte:existingUser.distanceFromShop}},{distanceTo:{$gte:existingUser.distanceFromShop}}]})
    .then(fin=>{

    console.log('FIN',fin)
    if(fin===null){

  return  res.send({ user: existingUser, token: tokenForUser(existingUser),rate:'We cannot deliver in your country' })
    }else{
        return  res.send({ user: existingUser, token: tokenForUser(existingUser),rate:fin })
    }
})
}


      });
    });
  },

  save(req,res){


      

        axios
                  .get("https://maps.googleapis.com/maps/api/geocode/json", {
                    params: {
                      address: req.body.postalCode + " " + req.body.deliveryAddress,
                      key: "INSERT KEY HERE",
                    },
                  }).then((rese)=>{
                    rese.data.results[0].geometry.location
                 

        User.findByIdAndUpdate(req.params.id,{$set:{name:req.body.body.name,
                                                    surname:req.body.body.surname,
                                                    groupUser:req.body.body.groupUser,
                                                    addresses:{
                                                                state:req.body.body.state,
                                                                city:req.body.body.city,
                                                                country:req.body.body.country,
                                                                postalCode:req.body.body.postalCode,
                                                                deliveryAddress:req.body.body.deliveryAddress,
                                                                mainAddress:req.body.body.mainAddress,
                                                                latitude:rese.data.results[0].geometry.location.lat,
                                                                longitude:rese.data.results[0].geometry.location.lng
                                                    }}}).then(resp=>{
                                                        console.log('RESP',resp)
                                                        res.status(200).send({result:'OK'})
                                                    })
                                                })

  }
};
