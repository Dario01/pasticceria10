const { db } = require("../database/model/Cake");
const Cake = require("../database/model/Cake");
const SalesPriceList = require("../database/model/SalesPriceList");
var MongoClient = require("mongodb").MongoClient;
var url = "mongodb://localhost:27017/";
const keys=require('../services/config')

module.exports = {
  reading(req, res) {
    
    

    //we have to do an aggregate between cakes and stocks

    MongoClient.connect(keys.MONGODB_URI.toString(), function (err,db){

      if(err) throw err;

      if(process.env.NODE_ENV==='production'){
        console.log('INSIDE NODE PRODUCTION')
        
        var dbo=db.db("cakkes")
      }else {
        console.log('OUTSIDE NODE PRODUCTION')
        var dbo=db.db("paste")
      }

      dbo.collection("stocks").aggregate([
        {
          $lookup:{
            from:"cakes",
            localField:"nameCake",
            foreignField:"name",
            as:"list",

          },
        },
      ])
        .toArray(function(err,resp){
          if (err) throw err;

          console.log('RESP',resp)

          return res.send({stocks:resp})
        })


    })



  },

  insert(req, res) {

    console.log("from fronte end", req.body);

    var arry = [];

    arry = req.body.body.map((ele) => ele.id);

    console.log("ARRY", arry);

    
    

      var promise = Promise.all(
        req.body.body.map((res) => {
          return new Promise((resolve, reject) => {
            
            

            const salespricelist = new SalesPriceList({
              cakeId: res.id,
              first: res.first,
              second: res.second,
              third: res.third,
              sales:res.activated,
              pubblication_date: new Date(),
              dailyProduction:res.daily_qty
            });

            salespricelist.save().then((r) => {
              
              resolve(r);
            });
          });
        })
      );


      //

      promise.then(() => {
        

        Cake.find({ _id: { $in: arry } }).then((result) => {

        MongoClient.connect(keys.MONGODB_URI.toString(), function (err, db) {
          if (err) throw err;

          if(process.env.NODE_ENV==='production'){
           
            var dbo=db.db("cakkes")
    
          }else{
          var dbo = db.db("paste");
          }

          dbo
            .collection("salepricelists")
            .aggregate([
              {
                $lookup: {
                  from: "cakes",
                  localField: "cakeId",
                  foreignField: "_id",
                  as: "listcakes",
                },
              },
            ])
            .toArray(function (err, resp) {
              if (err) throw err;
              
              return res.send({ sales: resp, result: result });
            
            });
        });
      });
    });
  },

  readingAuth(req,res){

    MongoClient.connect(keys.MONGODB_URI.toString(), function (err, db) {
      if (err) throw err;

      if(process.env.NODE_ENV==='production'){
        console.log('IN SALES ENV',process.env.NODE_ENV)
        var dbo=db.db("cakkes")

      }else{
      var dbo = db.db("paste");
      }

      dbo
        .collection("salepricelists")
        .aggregate([
          {
            $lookup: {
              from: "cakes",
              localField: "cakeId",
              foreignField: "_id",
              as: "listcakes",
            },
          },
        ])
        .toArray(function (err, resp) {
          if (err) throw err;
          return res.send({ sales: resp });
        });
    });


  },

  salesActivation(req,res){

    console.log('activation sales',req.body)

    const arrId=req.body.body.map(ele=>ele.id)

    const activateValue=req.body.body.map(ele=>ele.checked)

    console.log('ARRID',arrId)

    console.log('ARRIACTIVATE',activateValue)

    var promise=Promise.all(req.body.body.map(el=>{

      
      return new Promise ((resolve,reject)=>{
        
        
        SalesPriceList.update({cakeId:el.id},{sales:el.checked}).then(rei=>{


        resolve(rei)

        


      })

    })

  })
    )
    promise.then(re=>{
      console.log('RE',re)
      res.status(200).send({response:'Modify accepted'})
    })


    




  },

  delete(req, res) {},

  update(req, res) {},
};
