const express=require('express');
const CakeController = require('../controllers/CakeController');
const router=express.Router();
const IngredientController=require('../controllers/IngredientController');
const SalesPriceListController = require('../controllers/SalesPriceListController');
const CompanyController=require('../controllers/CompanyController');
const UserController=require('../controllers/Usercontroller');
const RatesController=require('../controllers/RatesController')
const stripe=require('stripe')('sk_test_W3BsIjiJHGSjYeVp5LaYmxsw00hBE5dGoU');
const OrderSchema=require('../database/model/Orders')
const User=require('../database/model/User');
const Stock=require('../database/model/Stock');
const Company = require('../database/model/Company');
const emailSenderOrder=require('../services/emailSenderOrder');
const ConfigurationController = require('../controllers/ConfigurationController');
const Configurations = require('../database/model/Configuration');


//ingredient

router.post("/ingredient",IngredientController.insert);

router.get("/ingredient", IngredientController.reading);

//se bisogna eliminare più di 1 elemento uso la post

router.delete("/ingredient/:id", IngredientController.delete);

router.put("/ingredient/:id", IngredientController.update);

//cake

router.post("/cake", CakeController.insert);

router.get("/cake",CakeController.reading);

router.delete("/cake/:id", CakeController.delete);

router.put("/cake/:id", CakeController.update);

//salespricelist

router.post("/salespricelist", SalesPriceListController.insert);

router.get("/salespricelist",SalesPriceListController.readingAuth);

router.delete("/salespricelist/:id", SalesPriceListController.delete);

router.put("/salespricelist/:id", SalesPriceListController.update);

//setSales on salespricelist

router.post("/salesActivation", SalesPriceListController.salesActivation)

//company

router.post("/companyData",CompanyController.insert);

router.get("/companyData", CompanyController.reading)

//configuration Data

router.post("/configurationData",ConfigurationController.insert)

router.get("/configurationData", ConfigurationController.reading)

//stripe

router.post("/stripe", (req,res)=>{

    console.log('REQ IN STRIPPPPE',req.body)
    console.log('BODY BASKET',req.body.body.basket)

    const charge= stripe.charges.create({

        amount:req.body.body.totalAmount * 100,
        currency:'eur',
        description:'Confirm Basket',
        source:req.body.body.token.id
    })

        // const orders=new OrderSchema({
        //     dateOrder:new Date(),
        //     detailOrder:req.body.body.basket
        // })

var promise=Promise.all(req.body.body.basket.map(elem=>{

    return new Promise((resolve,reject)=>{

    Stock.update({nameCake:elem.name, choice:elem.choice},{$set:{qty:elem.qty}}).then(respo=>{

        console.log('RESS IN STOCK UPDATE',respo)

        resolve(respo)
    }).catch((err)=>console.log('ERR UPDATED',err))

})

}))

    promise.then(()=>{
        

        User.update({email:req.body.body.token.email},
            {$push:{orders:{detailOrder:req.body.body.basket, dateOrder:new Date()}}}).then(resp=>{

           console.log('RES UPDATE',resp)
           emailSenderOrder(req.body.body.token.email, req.body.body.basket).then(re=>{

            console.log('RE',re)
           return res.status(200).send({order:'We have received your order'})

           }).catch(error=>console.log('SENDING ERROR ',error))
           
           
       }).catch(erro=>console.log('ERRO IN UPDATED USER',erro))
          
})


            
})  


    //update existing User

router.put("/personalData/:id", UserController.save)

//Rates Data

router.post("/ratesData", RatesController.insert)

router.delete("/ratesData/:id", RatesController.delete)

router.get("/ratesData", RatesController.reading)




module.exports=router;