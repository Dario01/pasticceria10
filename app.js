const express=require('express');
const route=require('./server/route/index')
const bodyParser=require('body-parser');
const mongoose=require('mongoose')
require('./server/services/cron_db')

// if(process.env.NODE_ENV!=='development '){

//     console.log('this is development')

//     mongoose.connect('mongodb://localhost/paste')
    
// }

const app=express();

app.use(bodyParser.json())

route(app)


module.exports=app